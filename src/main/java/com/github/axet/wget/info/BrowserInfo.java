package com.github.axet.wget.info;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * BrowserInfo - keep all information about browser
 * 
 */
public class BrowserInfo implements Serializable {
    private static final long serialVersionUID = 2603968158067492115L;

    public static String USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36";

    protected String userAgent;
    protected ProxyInfo proxy;

    public BrowserInfo() {
        userAgent = USER_AGENT;
    }

    synchronized public String getUserAgent() {
        return userAgent;
    }

    synchronized public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    synchronized public ProxyInfo getProxy() {
        return proxy;
    }

    synchronized public void setProxy(ProxyInfo proxy) {
        this.proxy = proxy;
    }

    /**
     * copy resume data from oldSource;
     */
    synchronized public void resume(BrowserInfo old) {
        proxy = new ProxyInfo(old.proxy);
        userAgent = old.userAgent;
    }

    public JSONObject save() {
        JSONObject o = new JSONObject();
        return o;
    }

    public void load(JSONObject o) {
    }
}
