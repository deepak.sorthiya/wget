package com.github.axet.wget;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

public class ApacheIndex extends HashMap<String, ApacheIndex.Entry> {
    public static SimpleDateFormat APACHE_DATE = new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.US);
    public static Pattern DATE_SIZE = Pattern.compile("(([0-9][0-9])-([a-zA-Z][a-zA-Z][a-zA-Z])-([0-9][0-9][0-9][0-9]) ([0-9][0-9]:[0-9][0-9]))[ ]+([0-9]+)");

    public long last;

    public static Date parse(SimpleDateFormat d, String s) {
        try {
            return d.parse(s);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class Entry {
        public String name; // a text, can be cut...>
        public Date date; // file date
        public long size; // file size

        public Entry(String n, Date d, long s) {
            name = n;
            date = d;
            size = s;
        }
    }

    public ApacheIndex() {
    }

    public ApacheIndex(String url) throws IOException {
        Document doc = Jsoup.connect(url).get();
        parse(doc);
    }

    public void parse(Document doc) throws IOException {
        Elements aa = doc.select("a");
        for (Element a : aa) {
            Node n = a.nextSibling();
            String t = n.toString();
            Matcher m = DATE_SIZE.matcher(t);
            if (m.find())
                put(a.attr("href"), new Entry(a.text(), parse(APACHE_DATE, m.group(1)), Long.valueOf(m.group(6))));
        }
        last = System.currentTimeMillis();
    }
}
